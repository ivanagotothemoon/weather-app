interface IGetWeather {
    lat: number,
    lon: number,
    url?: string
}
export const getWeather = async ({lat, lon}:IGetWeather, baseUrl: string = 'https://weatherbit-v1-mashape.p.rapidapi.com/current?') => {

    const url: string = `${baseUrl}lon=${lon}&lat=${lat}`

    try {
        return fetch( url, {
            "method": "GET",
            "headers": {
                "x-rapidapi-key": "39aa38beb1mshb4f69d5ac4cbc52p1e6280jsn715a0ee798cd",
                "x-rapidapi-host": "weatherbit-v1-mashape.p.rapidapi.com"
            }
        }).then( response => response.json())
    } catch (err) {
        console.error(err);
    }
}
export interface IGetAllWeather {
    city_name: string,
    datetime: string,
    weather: { description: string },
    data: any[],
}
export const getAllWeather = async ( locations: IGetWeather[] ): Promise <IGetAllWeather[] | undefined> => {
    const promises = locations.map( async location => {
       const res = await getWeather( location, "https://weatherbit-v1-mashape.p.rapidapi.com/forecast/daily?" ) 
       console.log(res)
       return res
    })
    const result:IGetAllWeather[] = await Promise.all(promises)
    return result
}