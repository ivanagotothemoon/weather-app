import React from 'react';
import './App.css';

interface ICityWeatherNestedProps {
    date: string,
    description: string,
    minTemp: number, 
    maxTemp: number
}

function CityWeatherNested({date, description, minTemp, maxTemp}:ICityWeatherNestedProps) {
  return (
     <li className="city-weather-nested-container col-12 col-sm-6 col-md-4 col-lg-4 col-xl-6 col-xxl-4">
         <div className="city-weather-nested">
            <h4 className="date">{date}</h4>
            <p className="description">{description}</p>
            <p className="min-max">min:<span className="min-temp">{minTemp}</span> | max:<span className="max-temp">{maxTemp}</span></p>
         </div>
    </li> 
  );
}
export default CityWeatherNested;


