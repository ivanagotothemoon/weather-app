import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { getAllWeather, IGetAllWeather } from './lib'
import {locations, dummyData} from './data'
import CityWeather from './CityWeather';

import CityWeatherNested from './CityWeatherNested';


function PageTwo() {
  const [data, setData] = useState([]);
  const [search, setSearch] = useState('');
  const [max, setMax] = useState(0)
  const [min, setMin] = useState(30)

  useEffect(() => {
    (async function(){
      const allData:any = await getAllWeather(locations)
      // const getData = async () => dummyData
      // const allData: any = await getData()

      setData( allData )
    }())
    
  },[])
  return (
    <div className="row">
      <ul className="header">
        <li>
          <Link to="/">PageOne</Link>
        </li>
        <li className="min-temp">
          <input type="range" id="min_temp" name="min_temp" min="0" max="30" value={min} onChange={(e)=>{
            setMin(Number(e.target.value))
          }} />
          <label htmlFor="min_temp">Min({min})</label>
        </li>
        <li className="max-temp">
          <input type="range" id="max_temp" name="max_temp" min="0" max="40" value={max} onChange={(e)=>{
            setMax(Number(e.target.value))
          }} />
          <label htmlFor="max_temp">Max({max})</label>
        </li>
        <li className="search">
          <input type="text" placeholder="search locations" onChange={ e => setSearch(e.target.value)} />
        </li>
      </ul>
      <hr />

      <h2>PageTwo</h2>  
      <ul className="location-container">
        {data.length > 0 && data.filter(loc => {
          console.log({loc})
          const cityName:string = loc['city_name']
          
          if(search === '') return true

          return cityName.toLowerCase().includes( search.toLocaleLowerCase() )
        })
        .map((loc:IGetAllWeather, idx) => <CityWeather 
            key={`page_two_location_${idx}`}
            cityName={loc['city_name']} 
        >
            <ul className="sixteen-week-container">
          {loc['data']
          .filter(({min_temp}) => min_temp <= min )
          .filter(({max_temp}) => max_temp >= max )
          .map( ({datetime, weather:{description}, max_temp, min_temp}, idx) =>

            <CityWeatherNested 
              key={`city_weather_nested_${idx}`}
              date={datetime}
              description={description}
              minTemp={min_temp}
              maxTemp={max_temp}
            />
          )}
          </ul>
        </CityWeather>)}
      </ul>
    </div>
  );
}

export default PageTwo;
