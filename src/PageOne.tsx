import React, { useState, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import {locations} from './data'
import { getWeather } from './lib'
import CityWeather from './CityWeather';
import { Link } from 'react-router-dom';


function PageOne() {
  const { lon, lat } = locations[0]

  const [data, setData] = useState([]);

  useEffect(() => {
    (async function(){
      // const res:any  = await getWeather( lat, lon )
      const getData = async () =>{
       return {"data":[{"rh":76,"pod":"n","lon":-0.13,"pres":1021,"timezone":"Europe\/London","ob_time":"2021-07-15 20:46","country_code":"GB","clouds":43,"ts":1626381960,"solar_rad":0,"state_code":"ENG","city_name":"London","wind_spd":1,"wind_cdir_full":"east-northeast","wind_cdir":"ENE","slp":1025.6,"vis":5,"h_angle":-90,"sunset":"20:10","dni":0,"dewpt":13,"snow":0,"uv":0,"precip":0,"wind_dir":69,"sunrise":"04:02","ghi":0,"dhi":0,"aqi":46,"lat":51.51,"weather":{"icon":"c02n","code":802,"description":"Scattered clouds"},"datetime":"2021-07-15:21","temp":17.2,"station":"F8149","elev_angle":-6.51,"app_temp":17.3}],"count":1}

      }
      const res:any = await getData()
      // console.log(res)
      setData( res.data)
    }())
    
  },[])
  return (
    <div className="row">
       <ul>
          <li>
            <Link to="/page-two">PageTwo</Link>
          </li>
        </ul>

        <hr />
      <h2>PageOne</h2> 
      <ul className="location-container">
        { data.length && <CityWeather 
          cityName={data[0]['city_name']} 
          description={data[0]['weather']['description']} 
        />}
      </ul>
    </div>
  );
}

export default PageOne;
