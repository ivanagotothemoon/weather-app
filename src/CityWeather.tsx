import React from 'react';
import './App.css';

interface ICityWeatherProps {
    cityName?: string,
    description?: string,
    children?:any
}

function CityWeather({cityName, description, children}:ICityWeatherProps) {
  return (
     <li className="city-weather-container col-12 col-sm-6 col-md-6 col-lg-6 col-xl-4 col-xxl-4">
         <div className="city-weather">
            <h4 className="city-name">{cityName}</h4>
            { description && <p className="description">{description}</p> }
            { children }
         </div>
    </li> 
  );
}
export default CityWeather;


