import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import PageOne from './PageOne';
import PageTwo from './PageTwo';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

function App() {
  return (
    <div className="App container-fluid">
    <Router>
      <div>


        
        <Switch>
          <Route exact path="/">
            <PageOne />
          </Route>
          <Route path="/page-two">
            <PageTwo />
          </Route>
          
        </Switch>
      </div>
    </Router>


    </div>
  );
}

export default App;
